module.exports = {
  /**
   * Transfer energyType to container 
   * @param {Creep} creep
   * @param {StructureContainer} container
   * @param {string} energyType
   */
  transfer: function(creep, container, energyType){
    let tranReturn = creep.transfer(containers[0], RESOURCE_ENERGY);
    if(tranReturn == OK){
      return(true);
    } else {
      if(Memory.debug){
        console.log('structure.container | 8 |', [false, tranReturn])
      }
      return([false, tranReturn]);
    }
  }
}