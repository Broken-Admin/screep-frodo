var structSpawn = require('structure.spawn');
var roleList    = require('role.list');

module.exports = {
    run: function() {
        // Clear memory to prevent a stack overflow
        for(let name in Memory.creeps) {
            if(!Game.creeps[name]) {
                delete Memory.creeps[name];
            }
        }
        for(var roleNum in roleList){
            let currentAmount = _.filter(Game.creeps, (creep) => creep.memory.role == roleList[roleNum].name).length;
            let desiredAmount = roleList[roleNum].num;
            let parts         = roleList[roleNum].parts;
            let name          = roleList[roleNum].name + Game.time;
            let role          = roleList[roleNum].name;
            if(currentAmount < desiredAmount){
                if(!structSpawn.spawnCreep(parts, name, role)[0] && Memory.debug){
                    console.log('tools.spawning | 20 | Could not spawn ' + name);
                }
            }
            
        }
        /* OLD
        let harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');

        if(harvesters.length < 4) {
            var name = 'Harvester' + Game.time;
            structSpawn.spawnCreep([WORK,CARRY,MOVE], name, 'harvester')
        }
    
        let upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');

        if(upgraders.length < 4 && harvesters.length != 0) {
        let name = 'Upgrader' + Game.time;
            structSpawn.spawnCreep([WORK,CARRY,MOVE], name, 'upgrader')
        }
        
        let builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder')
        
        if(builders.length < 2 && harvesters.length != 0) {
        let name = 'Builder' + Game.time;
            structSpawn.spawnCreep([WORK,CARRY,MOVE], name, 'builder')
        }
        */
    }
};