// Creep modules
var roleList      = require('role.list');
// Structure modules
var structSpawn   = require('structure.spawn');
// Misc modules
var spawning      = require('tools.spawning')

module.exports.loop = function () {
    spawning.run();
    
    if(Game.spawns['SpawnOE'].spawning) { 
        var spawningCreep = Game.creeps[Game.spawns['SpawnOE'].spawning.name];
        Game.spawns['SpawnOE'].room.visual.text(
            'Building - ' + spawningCreep.memory.name,
            Game.spawns['SpawnOE'].pos.x + 1, 
            Game.spawns['SpawnOE'].pos.y,
            {align: 'left', opacity: 0.8}
        );
    }

    for(var name in Game.creeps) {
        let creep = Game.creeps[name];
        for(role in roleList){
            if(creep.memory.role == roleList[role].name){
                roleList[role].req.run(creep);
            }
        }
    }
}