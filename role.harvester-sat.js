structContainer = require('structure.container');
roleBuilder = require('role.builder');
roleStationaryHarvester = {
	/** @param {Creep} creep **/
	run: function (creep) {
		let containers = creep.room.find(FIND_STRUCTURES, {
			filter: (structure) => {
				return (structure.structureType == STRUCTURE_CONTAINER && !(structure.store <= structure.storeCapacity));
			}
		});
		if (containers.length == 0) { // If there are no containers
			// Try to build container
			let targets = creep.room.find(FIND_CONSTRUCTION_SITES);
			if (creep.memory.building && creep.carry.energy == 0) {
				creep.memory.building = false;
			}
			if (!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
				creep.memory.building = true;
			}

			if (creep.memory.building) {
				if (targets.length) {
					for (let i in targets) {
						creep.build(targets[i]);
					}
				}
			} else {
				let sources = creep.room.find(FIND_SOURCES);
				creep.harvest(sources[1])
			}
		}
		if (creep.carry.energy >= creep.carryCapacity) {
			for (i in containers) {
				structContainer.transfer(creep, containers[i], RESOURCE_ENERGY)
			}
		} else if (creep.carry.energy < creep.carryCapacity) {
			var sources = creep.room.find(FIND_SOURCES);
			for (i in sources) {
				sReturn = creep.harvest(sources[i]);
				if (sReturn == ERR_NOT_IN_RANGE) {
					continue;
				} else if (sReturn == OK) {
					if (Memory.debug) {
						console.log('role.harvester-sat | 24 |', true);
					}
					continue
				} else if (Memory.debug) {
					console.log('role.harvester-sat | 28 |', [false, sReturn]);
					continue
				} else {
					continue;
				}

			}
			// After mining, if a container has 50,000 less hitpoints than max, repair it
			let damagedContainers = creep.room.find(FIND_STRUCTURES, {
				filter: (structure) => {
					return (structure.structureType == STRUCTURE_CONTAINER && structure.hits >= structure.hitsMax - 50000);
				}
			});
			for (i in damagedContainers) {
				creep.repair(damagedContainers[i]);
			}
			// After possibly repairing
		}
	}
};

module.exports = roleStationaryHarvester;