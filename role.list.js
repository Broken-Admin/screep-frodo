// -1 on parts conveys I am unfinished with this module
// 0 on num conveys the same

// 'Mini' means it is a smaller, less resource needy, version of a larger module
module.exports = [
    {
        name: 'harvester-mini',
        req: require('role.harvester'),
        num: 4,
        parts: [WORK,CARRY,MOVE,MOVE,MOVE,MOVE]
    },
    {
        name: 'harvester',
        req: require('role.harvester'),
        num: 1,
        parts: [WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE]
    },

    {
        name: 'builder-mini',
        req: require('role.builder'),
        num: 2,
        parts: [[WORK,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE]]
    },
    {
        name: 'builder',
        req: require('role.builder'),
        num: 2,
        parts: [WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE]
    },

    {
        name: 'upgrader-mini',
        req: require('role.builder'),
        num: 2,
        parts: [[WORK,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE]]
    },
    {
        name: 'upgrader',
        req: require('role.upgrader'),
        num: 1,
        parts: [WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE,MOVE]
    },

    {
        name: 'harvestSat',
        req: require('role.harvester-sat'),
        num: 0,
        parts: [WORK,WORK,WORK,CARRY,CARRY,CARRY]
    },
    {
        name: 'harvestNat',
        req: require('role.harvester-nat'),
        num: 0,
        parts: [CARRY,CARRY,CARRY,MOVE,MOVE,MOVE,MOVE]
    }
];