// Base for all creep roles
module.exports = {
    harvestPathColor: '#ffaa00',
    generalPathColor: '#ffffff',
    idlePathColor:    '#000000',
    storePathColor:   '#ffff00',
    attackPathColor:  '#00ff00'
};