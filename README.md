# screep-frodo
### Creep modules and tools I use when on the Frodo server

----

## Roles - Amount of creep under a role to be spawned are in `role.list.js`, alongside their parts

### Base
- Stores the colors of creep travel paths

### List
- Stores the parts, scripts, names and role names of screep roles, used by the main file

### Harvester
- Harvests from sources and brings the resources to a extension or spawn.
- Picks up any resources dropped.

### Harvester-Sat
- Stationary harvester, designed to be sat by a container
- Can repair and build containers
- Can only transfer to containers directly beside it

### Harvester-Nat
- Custom designed for myself, can be edited
- Designed to bring my Harvester-Sat to a resource
- Designed to take from containers the Harvester-Sat is filling and bring them to the spawn or extensions

### Upgrader
- Designed purely to upgrade the room controller

### Builder
- Designed to build on contruction sites

----

## Structures - Functions designed to make use of structures easier

### Spawn

1. getSpawns
   - Finds the spawns in the room of the creep passed

2. canSpawnRegular
   - Checks if the spawn passed can spawn a creep that costs less than 300 energy

3. spawnCreep
   - Attempts to spawn a creep with the passed role, name and parts
   - Returns boolean `true` if spawning was properly started
   - Returns an array with the boolean `false` as it's first index and error code as it's second if the spawning couldn't start
   - With debug mode, outputs errors in spawning and new spawnings to console
 
### Containers

1. transfer
   - Transfers resource from the creep passed to the container passed
   - Returns boolean `true` if the transfer was finished
   - If the transfer did not complete, returns an array with the boolean `false` as it's first index and the error code in it's second index
   - If debug mode is on console logs when there is an error

----

## Tools

### Spawning
 - Uses `role.list` for creep parts, amounts, roles
 - Checks if the amount of creeps supposed to be spawned is  furfilled
 - If the amount of creeps is insufficient, attempt to use `structures.spawn` to spawn a new creep of that type
