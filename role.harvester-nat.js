// Designed to work with the role.harvester-sat
var roleBase = require('role.base')
var roleStationaryHarvestHelper = {
	/** @param {Creep} creep **/
	run: function (creep) {
		let containers = creep.room.find(FIND_STRUCTURES, {
			filter: (structure) => {
				return (structure.structureType == STRUCTURE_CONTAINER && structure.store >= 200);
			}
		});
		for (i in containers) {
			while (creep.transfer(containers[i]) == ERR_NOT_IN_RANGE) {
				creep.moveTo(containers[i]);
			}
		}
		if (creep.carry.energy >= creep.carryCapacity) {
			let storages = creep.room.find(FIND_STRUCTURES, {
				filter: (structure) => {
					return (structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_SPAWN) &&
						structure.energy < structure.energyCapacity;
				}
			});
			if (storages.length > 0) {
				if (creep.transfer(storages[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
					creep.moveTo(storages[0], {
						visualizePathStyle: {
							stroke: roleBase.storePathColor
						}
					});
				}
			} else {
				let spawns = creep.room.find(FIND_STRUCTURES, {
					filter: (structure) => {
						return (structure.structureType == STRUCTURE_SPAWN)
					}
				});
				if (spawns.length > 0) {
					creep.moveTo(spawns[0], {
						visualizePathStyle: {
							stroke: roleBase.idlePathColor
						}
					});
				}
			}
		}
		let harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvestSat')
		if (harvesters.length >= 1) {
			let harvester = harvesters[0];
			if ([harvester.pos.x, harvester.pos.y].toString() != [14,43].toString()) {
				if (creep.pull(harvester) == ERR_NOT_IN_RANGE) {
					creep.moveTo(harvester);
				} else {
					if ([harvester.pos.x, harvester.pos.y].toString() != [14,43].toString()) { //14, 43 - station in W1N3
						if ([creep.pos.x, creep.pos.y].toString() == [14, 43].toString()) {
							creep.move(RIGHT);
						} else if (creep.pos.x != 15 || creep.pos.y != 43) {
							creep.moveTo(14, 43);
						}
						creep.pull(harvester);
						harvester.move(creep);
					}
				}
			} else if([creep.pos.x, creep.pos.y].toString() != [15, 42].toString()){
				creep.moveTo(15,42);
			}
		}
	}
};

module.exports = roleStationaryHarvestHelper;