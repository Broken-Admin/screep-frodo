var roleBase = require('role.base');
var roleHarvester = {

    /** @param {Creep} creep **/
    run: function (creep) {
        let sources = creep.room.find(FIND_SOURCES);
        let dropped = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES);
        if (creep.carry.energy == creep.carryCapacity) {
            creep.memory.storing = true;
        } else if (creep.carry.energy == 0) {
            creep.memory.storing = false;
        } else if (dropped) {
            creep.memory.storing = false;
            creep.memory.pickup  = true;
        } else if (!dropped) {
            creep.memory.pickup = false;
        }
        if (creep.carry.energy < creep.carryCapacity && !creep.memory.storing) {
            if (dropped) {
                console.log(dropped[0]);
                if (creep.pickup(dropped[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(dropped[0]);
                }
            }
            if (!creep.memory.pickup) {
                if (creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(sources[0], {
                        visualizePathStyle: {
                            stroke: roleBase.harvestPathColor
                        }
                    });
                }
            }
        } else {
            creep.memory.storing = true;
            let storages = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION || structure.structureType == STRUCTURE_SPAWN) &&
                        structure.energy < structure.energyCapacity;
                }
            });
            if (storages.length > 0) {
                if (creep.transfer(storages[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(storages[0], {
                        visualizePathStyle: {
                            stroke: roleBase.storePathColor
                        }
                    });
                }
            } else {
                let spawns = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_SPAWN)
                    }
                });
                if (spawns.length > 0) {
                    creep.moveTo(spawns[0], {
                        visualizePathStyle: {
                            stroke: roleBase.idlePathColor
                        }
                    });
                }
            }
        }
    }
};

module.exports = roleHarvester;