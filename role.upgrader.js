var roleBase = require('role.base');
var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.room.controller) {
            if(creep.carry.energy >= creep.carryCapacity){
                creep.memory.upgrading = true;
            } else if(creep.carry.energy == 0){
                creep.memory.upgrading = false;
            }
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_ENOUGH_RESOURCES || creep.carry.energy < creep.carryCapacity 
            && !creep.memory.upgrading){
                let sources = creep.room.find(FIND_SOURCES);
				if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                   	creep.moveTo(sources[0], {visualizePathStyle: {stroke: roleBase.harvestPathColor}});
                }
			}
            else if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE && (creep.carry.energy >= creep.carryCapacity || creep.memory.upgrading)) {
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: roleBase.generalPathColor}});
            } 
        }
    }
}
module.exports = roleBuilder;