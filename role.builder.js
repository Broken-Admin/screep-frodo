var roleBase = require('role.base');
var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {
		let targets = creep.room.find(FIND_CONSTRUCTION_SITES);
	    if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
	    }
	    if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.building = true;
	    }

	    if(creep.memory.building) {
            if(targets.length) {
                if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: roleBase.generalPathColor}});
                }
            }
	    }
	    else {
	        let sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
				creep.moveTo(sources[0], {visualizePathStyle: {stroke: roleBase.harvestPathColor}});
            }
		}
	}
};

module.exports = roleBuilder;