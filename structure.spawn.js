module.exports = {
    getSpawns: function(creep){
        let spawns = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return structure.structureType == STRUCTURE_SPAWN;
            }
        });
        return(spawns);
    },
    canSpawnRegular: function(name) {
        if(Game.spawns[name].energy >= 300){
            return(true);
        }
        return(false);
    },
    spawnCreep: function(parts, name, role) {
        let spawns = [];
        for(s in Game.spawns){
            spawns.push(s);
        }
        
        let spawnAttempt = Game.spawns[spawns[0]].spawnCreep( parts, name, 
        { memory: { role: role, name: name } } );
        if( spawnAttempt == OK ) {
            if(Memory.debug) {
                console.log('Spawning new ' + role + ': ' + name);
            }
            return(true);
        } else {
            if(Memory.debug) {
                console.log('structure.spawn | 31 |', [false, spawnAttempt]);
            }
            return([false, spawnAttempt]);
        }
    }
};